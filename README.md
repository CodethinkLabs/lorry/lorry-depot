# Lorry Depot

This project provides a configuration for Lorry Controller with a
dedicated Downstream Host.

It requires a server host running Debian 10, which will be further
configured using Ansible 2.9 or later.

## Variables

The Ansible playbook and roles use these variables, which can be set
in the inventory:

* `DEPOT_HOSTNAME`: Public hostname for the server, included in the
  repository URLs displayed by cgit.  No default value.
* `DEPOT_ROOT`: Base directory for mirror repositories.  This will be
  created if necessary.  Default: `/srv/lorry-depot`.
* `DEPOT_LORRY_WORKING_AREA`: Base directory for Lorry's working-area,
  a cache that should persist across container upgrades.
* `DEPOT_OWNER`: Name of the user and group that will own the mirror
  repositories and working-area.  This will be created if necessary.
  Default: `lorry`.
* `DEPOT_LC_HTTP_LOCAL_PORT`: Port number to bind on the server's
  loopback address, that will forward to Lorry Controller's HTTP
  service.  Default: `8080`.
* `DEPOT_CONFGIT_URL`: The URL from which Lorry Controller will fetch
  its CONFGIT repository.
* `DEPOT_LORRY_MANAGEMENT_PUBLIC_KEY`: A list of public ssh keys for
  accessing the Lorry Controller Administration interface.  These can
  be either key strings or filenames.
  The keys will be installed in /etc/lc-site/ssh/authorized_keys,
- `unattended_upgrades`: Flag for whether to enable automatic upgrades
  of Debian packages and Docker containers.  Default: `no`.
- `letsencrypt_certs`: Flag for whether to generate and install
  SSL/TLS server certificates using Let's Encrypt and certbot.  You
  should review the Let's Encrypt Subscriber Agreement and policies at
  <https://letsencrypt.org/repository/> before enabling this.
  Default: `no`.
- `letsencrypt_email`: Contact address to provide to Let's Encrypt.
  No default value.
